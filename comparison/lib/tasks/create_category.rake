namespace :create_category do

    require ENV['PWD'] + "/app/controllers/concerns/create_new_category"
    include CreateCategory

    task :create_root => :environment do 
        create()
    end
    

end