namespace :elasticsearch do

    require ENV['PWD'] + "/app/controllers/concerns/elasticsearch.rb"
    include ElasticSearch

    task :add_document => :environment do
        add_documents()
    end

    task :search_document => :environment do
        search_documents(ENV['QUERY'])
    end

end