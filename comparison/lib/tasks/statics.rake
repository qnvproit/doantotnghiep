namespace :statics do

    require ENV['PWD'] + "/app/controllers/concerns/statics"
    include Statics

    task :get_max_vocabulary => :environment do 
        get_max_vocabulary()
    end


end