namespace :update_statics_table do
    

    require ENV['PWD'] + "/app/controllers/concerns/update_statics_table"
    include UpdateStaticsTable


    task :update => :environment do 
        update()
    end

    task :update_ver2 => :environment do
        update_ver2()
    end



end
