namespace :mapping do

    require ENV['PWD'] + "/app/controllers/concerns/mapping"
    include Mapping

    task :test_samsung => :environment do 
        test_samsung()
    end

    task :test_mapping => :environment do
        test_mapping()
    end

    task :test_mapping_shop => :environment do
        test_mapping_shop()
    end

    task :fix_tgdd => :environment do
        fix_tgdd()
    end

end