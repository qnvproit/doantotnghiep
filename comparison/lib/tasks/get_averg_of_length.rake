namespace :get_averg_of_length do 

    require ENV['PWD'] + "/app/controllers/concerns/get_averg_length"
    include GetAvergOfLength

    task :compute => :environment do 
        get_averg()
    end

end