class Trainedcategorystring
    include Mongoid::Document

    field :category, type: String
    field :stringtotal, type: String
    field :numberword, type: Integer
    field :length, type: String
    field :probality, type: Float

    store_in collection: "trainedcategorystring"

    def get_cate
        self.category
    end

end
