class Category
    include Mongoid::Document

    field :link, type: String
    field :name, type: String
    field :parent, type: String 
    field :level, type: Integer

    store_in collection: "category3"
    

    def self.get_category_by_level(level)
        self.any_of({level: level})
    end

    def self.get_root_category
        self.any_of({})
    end

    def get_sub_categorys(limit=nil)
        sub_cates = Category.any_of({:parent => self.name})
        sub_cates = sub_cates.limit(limit) if not limit.nil?
        return sub_cates
    end


    # def get_list_leaf_category(cate)
    #     list_sub_cates = Array.new

    #     sub_cates = Category.any_of({parent: cate})
    #     if sub_cates.blank?
    #         list_sub_cates.push(cate)
        

    # end


    def get_productions_by_cate(cate)
        # Get list leaf-category (sub of this cate)


    end

end
