class Khuyenmai
    include Mongoid::Document

    field :title, type: String
    field :link, type: String
    field :image, type: String
    field :status, type: Integer

    store_in collection: "khuyenmai"
end