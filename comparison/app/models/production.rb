class Production
    include Mongoid::Document

    field :shop, type: String
    field :name, type: String
    field :price, type: String
    field :link, type: String
    field :image, type: String
    field :category, type: String
    field :description, type: String



    def standard_price 
        (self.price == "[]") ? "Liên hệ" : self.price.tr('[""]', '')
    end

    def standard_name
        self.name.tr('[""]', '')
    end

    def standard_image
        self.image.tr('[""]', '')
    end

    def id
        self._id
    end

    def parent_cate
        self.category.split('___')[0]
    end

    def single_cate
        self.category.split('___')[1]
    end

    def relate_product
        Production.where({:category => self.category}).limit(10)
    end

end
