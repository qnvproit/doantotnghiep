class Trainingdatum
    include Mongoid::Document

    field :category, type: String
    field :name, type: String


    store_in collection: "trainingdata"

end
