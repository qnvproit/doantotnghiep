class StaticTable
    include Mongoid::Document

    field :prev_w, type: String
    field :next_w, type: String
    field :number, type: Integer
    field :is_first, type: Integer
    field :category, type: String

    store_in collection: "statics_table_2"

end
