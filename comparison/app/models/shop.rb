class Shop
    include Mongoid::Document

    field :name_id, type: String
    field :name, type: String
    field :homepage, type: String
    field :brand_img, type: String

    store_in collection: "shop"

    def full_brand_img
        "/images/shop/" + self.brand_img
    end

    def self.shop_by_name(name_id)
        Shop.where({:name_id => name_id}).first
    end

end