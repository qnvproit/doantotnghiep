class Samsung
    include Mongoid::Document

    field :name, type: String
    field :price, type: String
    field :shop, type: String
    field :shop, type: String

    store_in collection: "smartphone_samsung"
end