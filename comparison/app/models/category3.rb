class Category3
    include Mongoid::Document

    field :link, type: String
    field :name, type: String
    field :parent, type: String 
    field :level, type: Integer

    store_in collection: "category3"

end
