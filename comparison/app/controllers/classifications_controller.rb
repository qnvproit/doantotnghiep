class ClassificationsController < ApplicationController


    @@IRGNOTE_WORD = ['-', ',', '', '/', '\\', '"', "'", ":", ".", "+", "=", " "]


    def test_class
        result = 0
        result_class = Array.new

        # Get list production in training data
        class1 = Trainingdatum.all[20000..20100]
        class1.each do |item|
            p_cate = self.class_by_method_3(item.name)
            if p_cate == item.category
                result += 1
                
            end
            result_class.append({ :name => item.name, :p_cate => p_cate} )
        end

        # render json: result_class
        render plain: result

    end


    def class_by_method_1(test_input)
        # Naive Bayes

        # test_input = "Tủ sắt CA-1BLG1"
        list_word_input = test_input.downcase.split(" ")

        max_p = 0
        max_cate = ""

        list_cate = Trainedcategorystring.all

        list_cate.each do |cate|
            list_word_sample = cate.stringtotal.split(" ")

            temp_p = cate.probality.to_f

            list_word_input.each do |w|
                if @@IRGNOTE_WORD.count(w) == 0
                    number_w_in_sample = list_word_sample.count(w)
                    temp_p *= (number_w_in_sample + 1) * 1.0 / (cate.length.to_i + cate.numberword.to_i)
                end
            end

            if temp_p > max_p
                max_p = temp_p
                max_cate = cate.category
            end
        end
        return max_cate
        # render plain: max_cate
    end


    def class_by_method_2(test_input)
        # Naive Bayes with log

        # test_input = "SG225 (SG225H) ghế xoay lưng trung hòa phát"
        list_word_input = test_input.downcase.split(" ")

        max_p = 0
        max_cate = ""

        list_p = Array.new

        list_cate = Trainedcategorystring.all

        list_cate.each do |cate|
            list_word_sample = cate.stringtotal.split(" ")

            temp_p = -Math.log2(cate.probality.to_f)

            list_word_input.each do |w|
                if @@IRGNOTE_WORD.count(w) == 0
                    number_w_in_sample = list_word_sample.count(w)
                    temp_p *= (number_w_in_sample + 1) * 1.0 / (cate.length.to_i + cate.numberword.to_i)
                end
            end
            list_p.append({ :cate => cate.category, :p => temp_p })
            if temp_p > max_p 
                max_p = temp_p
                max_cate = cate.category
            end
        end
        return max_cate
        # render json: list_p
        # render plain: max_cate
    end


    def class_by_method_3(test_input)
        # Naiva Bayes with unbalanced trained 

        # test_input = "SG225 (SG225H) ghế xoay lưng trung hòa phát"
        list_word_input = test_input.downcase.split(" ")

        max_p = 0
        max_cate = ""

        list_p = Array.new

        list_cate = Trainedcategorystring.all

        list_cate.each do |cate|
            list_word_sample = cate.stringtotal.split(" ")

            temp_p = cate.probality.to_f

            list_word_input.each do |w|
                if @@IRGNOTE_WORD.count(w) == 0
                    number_w_in_sample = list_word_sample.count(w)
                    temp_p *= ((number_w_in_sample + 1) * 1.0 / (cate.length.to_i + cate.numberword.to_i))**(1.0 / cate.length.to_i)
                end
            end
            list_p.append({ :cate => cate.category, :p => temp_p })
            if temp_p > max_p 
                max_p = temp_p
                max_cate = cate.category
            end
        end
        return max_cate
        # render json: list_p
        # render plain: max_cate
    end



end
