module Mapping
    require "unicode_utils/downcase"


    def test_samsung 
        puts "Starting ..."
        
        # puts compute_keywords_by_shop("phucanh")
        # puts diff_two_name "Samsung Galaxy S7 Edge - Vàng", "Samsung Galaxy J7 J700 - Đen"

        # lists = Samsung.where({:shop => "mediamart"})
        # lists.each do |t|
        #     puts t.name
        # end


        total_keywords = []

        list_shops = Samsung.all.distinct(:shop)

        list_shops.each do |shop|
            if shop != "adayroi"
                total_keywords = total_keywords + compute_keywords_by_shop(shop)
            end
        end

        puts total_keywords.uniq

        File.open("keywords.txt", "w") { |file| file.write(total_keywords.uniq.to_json) }

    end

    def test_mapping
        puts check_mapping_two_name("samsung s6 plus", "Điện thoại samsung s6 PLUS")
    end

    def test_mapping_shop
        mapping_shop('pico', 'thegioididong')
    end


    def fix_tgdd
        items = Samsung.where({:shop => "thegioididong"})
        items.each do |item|
            item.name = item.name.first
            item.price = item.price.first

            item.save()
        end
    end


    private
        def compute_keywords_by_shop(shop)
            keywords = []

            list_prods = Samsung.where({:shop => shop}).map(&:name)
            
            list_name = standart_list_name(list_prods)

            max_size = list_name.size

            list_name.each_with_index do |name, index|
                index.upto(max_size - 1) do |i|
                    keywords = keywords + diff_two_name(name, list_name[i])
                end
            end

            return keywords.uniq - black_words

            # print list_prods
            # print standart_list_name(list_prods)

            # max_size = list_prods.size

            # list_prods.each_with_index do |prod, index|
            #     index.upto(max_size - 1) do |i|
            #         keywords = keywords + diff_two_name(prod.name, list_prods[i].name)
            #     end
            # end

            # return keywords.uniq - black_words
        end


        def compute_keywords_by_shop_ver2(shop)
            keywords = []

            list_prods = Samsung.where({:shop => shop})

            max_size = list_prods.size

            list_prods.each_with_index do |prod, index|
                index.upto(max_size - 1) do |i|
                    keywords = keywords + diff_two_name(prod.name, list_prods[i].name)
                end
            end

            return keywords.uniq - black_words
        end


        
        def diff_two_name(name1, name2)
            list_term_first = standart_name(name1).split(" ")
            list_term_second = standart_name(name2).split(" ")

            return (list_term_first - list_term_second) + (list_term_second - list_term_first)
        end


        def standart_list_name(list_name)
            list_name.map{ |name|
                standart_name(name)
            }
        end

        def standart_name(str)
            UnicodeUtils.downcase(str).gsub("/", " ")
            .gsub(")", " ")
            .gsub("(", " ")
            .gsub("-", " ")
            .gsub(",", " ")
            .gsub("[", " ")
            .gsub("]", " ")
            .gsub('"', " ")
            .gsub("'", " ")
            .gsub("\\n", " ")
            .gsub("\\t", " ")
        end


        def black_words
            arr_cates = [
                "Điện thoại - Máy tính bảng",
                "Điện thoại di động",
                "Samsung",
            ]

            UnicodeUtils.downcase(arr_cates.join(" ")).split(" ")
        end


        def check_mapping_two_name(name1, name2)
            list_term_first = standart_name(name1).split(" ")
            list_term_second = standart_name(name2).split(" ")


            keywords = ["s7","edge","vàng","j7","j700","đen","s6","g925","j5","j500","trắng","grand","prime","ve","sm","g531","note","5","g920","j1","v","plus","g318h","core","ss","g360","a8","a800","a5","a510","a7","a710","g928","g361","xám","i8552","win","galaxy","htc","desire","326g","dual","sim","bạc","j2","e1272","j12h","ssj120b","ssj120w","ssj120g","hồng","j3","ssj320","j200gu","galaxyj2","j105","2016","gold","5.5inch","16gb","white","4.7inch","8gb","2","5.1inch","32gb","1","4","n910c","black","5.7inch","5.0inch","pink","green","64gb","a3","4.5inch","g531h","grey","e7","e700","g530h","e5","e500","5.2inch","j100h","4.0inch","4gb","duos","i8262","blue","4.3inch","g318","silver","a8ve","mini","e1200"]

            term1 = keywords - list_term_first
            term2 = keywords - list_term_second

            ((term1 - term2).blank? or (term2 - term1).blank?) ? true : false

        end


        def mapping_shop(shop1, shop2)
            prods_shop1 = Samsung.where({:shop => shop1}).map(&:name)
            prods_shop2 = Samsung.where({:shop => shop2}).map(&:name)

            result = Hash.new

            prods_shop1.each do |name1|
                t1 = standart_name(name1)
                result[name1] = Array.new

                prods_shop2.each do |name2|
                    # puts diff_two_name(name1, name2)
                    if check_mapping_two_name(name1, name2)
                        # result[name1].append(standart_name(name2))
                        puts name1 + " ---------------- " + name2
                    else


                    end
                end
            end

            # puts result

            # File.open("result_mapping.txt", "w") { |file|  file.write(result.to_json) }

        end

end