module ElasticSearch


    def add_documents
        docs = Production.all
        
        docs.each do |d|
            puts JSON.parse(d.name).first
            # add_document(d)
        end
        
    end


    def search_documents(query)
        puts "Searching: " + query
        search_document(query)
    end



    private 
        # index a new production
        def add_document(doc)
            d = {
                category: doc.category,
                name: JSON.parse(doc.name).first,
                shop: doc.shop
            }

            http = Curl.put("http://localhost:9200/price_checker/production/" + doc.id, d.to_json)
            puts http.body_str
        end
        

        # Search
        def search_document(query)
            result = Curl.get("http://localhost:9200/price_checker/production/_search?_source=false&q=name:" + query.strip)
            puts JSON.pretty_generate(result.body_str)
        end


end