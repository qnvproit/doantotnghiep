module CreateCategory

    def create_level_2
        puts "Create"

        if Category3.any_of({:link => "http://weWWvn/phu-kien-may-in/cat-556.htm"}).count != 0
            puts "OK"
        else
            puts "NOT"
        end
        # root_cates = Category.any_of.not({:name => /.*___.*/}).pluck(:name)

        # list_cates = Category.any_of({:name => /.*___*./})


        # list_cates.each do |cate|
        #     temp = cate.name.split("___")
        #     parent = temp[0]
        #     current = temp[1]

        #     if root_cates.include? parent
        #         Category3.create(
        #                     :name => current,
        #                     :link => cate.link,
        #                     :parent => parent,
        #                     :level => 2)
        #     end
        # end

    end


    def create_level_3
        puts "Create"

        root_cates = Category3.any_of({:level => 2}).pluck(:name)
        
        list_cates = Category.any_of({:name => /.*___*./})


        list_cates.each do |cate|
            temp = cate.name.split("___")
            parent = temp[0]
            current = temp[1]

            if root_cates.include? parent
                Category3.create(
                            :name => current,
                            :link => cate.link,
                            :parent => parent,
                            :level => 3)
            end
        end

    end


    def create_level_11
        puts "Create"

        root_cates = Category3.any_of({:level => 10}).pluck(:name)
        
        list_cates = Category.any_of({:name => /.*___*./})


        list_cates.each do |cate|
            temp = cate.name.split("___")
            parent = temp[0]
            current = temp[1]

            if root_cates.include? parent
                Category3.create(
                            :name => current,
                            :link => cate.link,
                            :parent => parent,
                            :level => 11)
            end
        end

    end


    def create_level(level)
        puts "_____________________________________________"
        puts "Create " + level.to_s

        root_cates = Category3.any_of({:level => (level-1)}).pluck(:name)
        
        list_cates = Category.any_of({:name => /.*___*./})


        list_cates.each do |cate|
            temp = cate.name.split("___")
            parent = temp[0]
            current = temp[1]

            if root_cates.include? parent

                if Category3.any_of({:link => cate.link}).count == 0

                    Category3.create(
                                :name => current,
                                :link => cate.link,
                                :parent => parent,
                                :level => level)
                end
            end
        end

    end


    def create
        2.upto(15) do |index|
            create_level(index)

        end
    end

end