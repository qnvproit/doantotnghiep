module ComputePrecision


    def compute(category)
        # puts find_score("ASUS A556UF-XX062D", "Laptop___Asus")
        # puts find_score("ASUS A556UF-XX062D", "Tivi - Âm thanh___Multimedia")

        # puts get_relation("asus", nil, "Laptop___Asus")
        # puts get_relation("asus", nil, "Tivi - Âm thanh___Multimedia")
        # puts find_category("Tủ mạng Netone NET-PD-3601B")

        list_prods = Trainingdatum.where({:category => category})
        temp = list_prods.size
        puts temp


        true_item = 0

        list_prods.each do |p|

            # Find category of production 'p'
            # Then compare with 'category'
            if find_category(p.name) == p.category
                true_item += 1
            else 
                puts "-----------------------------------------------"
                puts
            end

        end

        puts true_item.to_s + " / " + temp.to_s


    end

    private

        def standard_str(str)
            str.gsub(')','')
            .gsub('(','')
        end

        
        def count_records_in_category(category)
            return Trainingdatum.where({:category => category}).size
        end



        def get_relation(prev_w, next_w, category)

            if next_w.nil?
                item = StaticTable.where({
                                    :prev_w => prev_w,
                                    :is_first => 1,
                                    :category => category})
                puts item
            else
                item = StaticTable.where({
                                :prev_w => prev_w,
                                :next_w => next_w,
                                :category => category})
            end

            if item.size != 0
                # Count number record in category
                # number = Trainingdatum.where({:category => category}).size

                return (item.first.number.to_f / count_records_in_category(category) + 1)

            else
                return 1

            end
        end


        def find_score(name, category)
            arr_words = standard_str(name).downcase.split(" ")

            index = 0
            score = 1

            arr_words.each do |w|
                if index == 0

                    score = score * get_relation(w, nil, category)

                else
                    score = score * get_relation(arr_words[index - 1], w, category) 

                end
                index += 1
            end

            return score

        end


        def find_category(name)

            list_cate = Trainingdatum.distinct("category")

            hash_score = Hash.new

            list_cate.each do |c|
                hash_score[c] = 1
            end





            arr_words = standard_str(name).downcase.split(" ")


            index = 0

            arr_words.each do |w|

                if index == 0
                    items = StaticTable.where({:prev_w => w, :is_first => 1})

                    present_cates = items.distinct(:category)

                    hash_score.each do |k, v|
                        if present_cates.include? k
                            hash_score[k] = hash_score[k] * get_relation(w, nil, k) 
                        end
                    end


                    # items.each do |i|
                    #     hash_score[i.category] *= ( get_relation(w, nil, i.category) / count_records_in_category(i.category) + 1 )
                    # end

                else
                    items = StaticTable.where({:prev_w => arr_words[index - 1], :next_w => w})
                    present_cates = items.distinct(:category)

                    hash_score.each do |k, v|
                        if present_cates.include? k
                            hash_score[k] = hash_score[k] * get_relation(arr_words[index - 1], w, k)
                        end
                    end

                end

                index += 1
            end


            # puts hash_score

            # puts "Laptop___Asus" + hash_score["Laptop___Asus"].to_s

            return hash_score.max_by{|k,v| v}[0]

        end


end