class ApplicationController < ActionController::Base
    # Prevent CSRF attacks by raising an exception.
    # For APIs, you may want to use :null_session instead.
    protect_from_forgery with: :exception

    before_action :get_list_category

    private
        def get_list_category
            @list_cates = Category.get_category_by_level(1)
        end


end
