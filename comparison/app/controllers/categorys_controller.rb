class CategorysController < ApplicationController



    def index

    end


    def detail
        @category = Category.find(params[:id])
        @sub_cates = @category.get_sub_categorys

        if @category.parent.nil?
            @full_cate = @category.name
        else
            @full_cate = @category.parent + "___" + @category.name
        end

        @productions = Production.any_of({:category => @full_cate}).limit(10)
    end

end
