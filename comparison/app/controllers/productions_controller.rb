class ProductionsController < ApplicationController


    def home

        # Get list "Smartphone"
        @smartphones = list_by_category("Điện thoại di động___")
        @number_smtp = number_by_category("Điện thoại di động___")

        @laptops = list_by_category("Laptop___")
        @number_laptop = number_by_category("Laptop___")

        @mevabes = list_by_category("Mẹ và bé___")
        @number_mevabes = number_by_category("Mẹ và bé___")

        @thoitrangs = list_by_category("Thời trang - Mỹ phẩm___")
        @number_thoitrangs = number_by_category("Thời trang - Mỹ phẩm___")

        @tivis = list_by_category("Tivi___")
        @number_tivis = number_by_category("Tivi___")

        @khuyenmai = Khuyenmai.any_of({status: 1})

        render "home"
    end


    def detail_production
        @production = Production.find(params[:id])
    end


    def list
        @list = Production.all
    end

    private
        def list_by_category(cate)
            Production.any_of({:category => /.*#{cate}.*/}).limit(10)
        end

        def number_by_category(cate)
            Production.any_of({:category => /.*#{cate}.*/}).count().to_int()
        end

end
